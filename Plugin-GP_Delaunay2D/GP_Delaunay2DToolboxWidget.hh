//=============================================================================
//
//  CLASS GP_Delaunay2DToolboxWidget
//
//  $Date$
//
//=============================================================================

#include "ui_GP_Delaunay2DToolbox.hh"
#include <QtGui>

class GP_Delaunay2DToolboxWidget : public QWidget, public Ui::GP_Delaunay2DToolbox
{
  Q_OBJECT

  public:
    GP_Delaunay2DToolboxWidget(QWidget *parent = 0);
};
