#include <queue>

#include "GP_Delaunay2DPlugin.hh"
#include "OpenFlipper/BasePlugin/PluginFunctions.hh"

//-----------------------------------------------------------------------------

void
GP_Delaunay2DPlugin::
initializePlugin()
{
  gui_ = new GP_Delaunay2DToolboxWidget();
  QSize size(100, 100);
  gui_->resize(size);

  connect(gui_->pushButton_createInitialMesh, SIGNAL(clicked()), this, SLOT(slotCreateInitialMesh()));
  connect(gui_->pushButton_2DView,            SIGNAL(clicked()), this, SLOT(slotSet2DView()));

  emit addToolbox( tr("GP 2D Delaunay") , gui_ );
}

//-----------------------------------------------------------------------------

void
GP_Delaunay2DPlugin::
pluginsInitialized()
{
  // Define a new picking mode
  emit addPickMode("Separator");
  emit addPickMode("GP_Delaunay2D");
}

//-----------------------------------------------------------------------------

void
GP_Delaunay2DPlugin::
slotMouseEvent( QMouseEvent* _event )
{
  // control modifier is reserved for selecting target
  if ( _event->modifiers() & (Qt::ControlModifier) )
    return;

  if( _event->type() != QEvent::MouseButtonPress)
    return;

  if ( PluginFunctions::pickMode()   == ("GP_Delaunay2D") &&
       PluginFunctions::actionMode() == Viewer::PickingMode  )
  {
    // handle mouse events

    if ( _event->button() == Qt::LeftButton )
    {
      size_t     node_idx, target_idx;
      ACG::Vec3d       hit_point;


       // first, check if we picked an edge
      if (PluginFunctions::scenegraphPick(ACG::SceneGraph::PICK_EDGE, _event->pos(),node_idx, target_idx, &hit_point))
      {
        BaseObjectData* obj;
        if ( PluginFunctions::getPickedObject(node_idx, obj) )
        {
          // is picked object a triangle mesh?
          TriMeshObject * meshObject = PluginFunctions::triMeshObject(obj);

          if (meshObject)
          {
            TriMesh::EdgeHandle eh = meshObject->mesh()->edge_handle(target_idx);
            if (eh == TriMesh::InvalidEdgeHandle)
              return;

            // insert the respective point into the triangulation
            insertPoint(meshObject, true, TriMesh::InvalidFaceHandle, eh, hit_point);
            return;
          }
        }
      } // end of scenegraph edge picking


      // if we haven't picked an edge, pick a face
      if (PluginFunctions::scenegraphPick(ACG::SceneGraph::PICK_FACE, _event->pos(),node_idx, target_idx, &hit_point))
      {
        BaseObjectData* obj;
        if ( PluginFunctions::getPickedObject(node_idx, obj) )
        {
          // is picked object a triangle mesh?
          TriMeshObject * meshObject = PluginFunctions::triMeshObject(obj);

          if (meshObject)
          {
            QString name = meshObject->name();

            TriMesh::FaceHandle fh = meshObject->mesh()->face_handle(target_idx);
            if (fh == TriMesh::InvalidFaceHandle)
              return;

            // insert the respective point into the triangulation
            insertPoint(meshObject, false, fh, TriMesh::InvalidEdgeHandle, hit_point);
            return;
          }
        }
      } // end of scenegraph face picking


    } // end of if left button
  }

  emit updateView();
}

//-----------------------------------------------------------------------------

void
GP_Delaunay2DPlugin::
insertPoint(TriMeshObject * _meshObject, const bool _onEdge, const TriMesh::FaceHandle& _fh, const TriMesh::EdgeHandle& _eh, const TriMesh::Point& _p)
{
  if (_meshObject == 0)
    return;

  TriMesh * mesh = _meshObject->mesh();

  // add vertex, assign random color to it
  TriMesh::Point p = TriMesh::Point(_p[0], _p[1], 0.0);
  TriMesh::VertexHandle vh = mesh->add_vertex(p);

  // add a cone to visualize the corresponding voronio cell
  ACG::SceneGraph::GlutPrimitiveNode * node = getGlutPrimitiveNode(_meshObject);
  ACG::Vec3d axis = ACG::Vec3d(0,0,1);
  ACG::Vec3d offset = ACG::Vec3d(0,0,-1); // add an offset to the cones position, since the position is not located at the apex but at the intersection of axis and base plate
  node->add_primitive(ACG::SceneGraph::GlutPrimitiveNode::CONE, p+offset, axis, getRandomColor());


  if (_onEdge && _eh != TriMesh::InvalidEdgeHandle)
  {
    mesh->split(_eh, vh);
    std::cout << "[GP_Delaunay2DPlugin] 2:4 Split: vertex " << vh.idx() << " at position " << p << " on edge " << _eh.idx() << std::endl;
  }
  else if (_fh != TriMesh::InvalidFaceHandle)
  {
    mesh->split(_fh, vh);
    std::cout << "[GP_Delaunay2DPlugin] 1:3 Split: vertex " << vh.idx() << " at position " << p << " inside triangle " << _fh.idx() << std::endl;
  }
  else
    return;


  // INSERT CODE:
  // re-establish Delaunay property
  // ... find edges opposite to the inserted vertex
  // ... are these edges ok? otherwise: flip'em
  //--- start strip ---
  
  // circulate around the current vertex
  /*for (TriMesh::VertexEdgeIter ve_it=mesh->ve_iter(vh); ve_it.is_valid(); ++ve_it)
  {
     printf("Edge Found\n");
     if(mesh->is_flip_ok(*ve_it))
     {
       mesh->flip(*ve_it);
       printf("Flipped\n");
     }
  }*/

  for (auto h_it=mesh->halfedges_begin(); h_it!=mesh->halfedges_end(); ++h_it)
  {
  	auto eh = mesh->edge_handle(*h_it);
	if(vh == mesh->opposite_he_opposite_vh(*h_it) && !isDelaunay(mesh, eh) && mesh->is_flip_ok(eh)) {
	  mesh->flip(eh);		
	}
  }

  //--- end strip ---

  mesh->garbage_collection();

  emit updatedObject(_meshObject->id(), UPDATE_ALL);
}

//-----------------------------------------------------------------------------

bool
GP_Delaunay2DPlugin::
isDelaunay(TriMesh * _mesh, TriMesh::EdgeHandle _eh)
{
  if (_mesh == 0)
      return false;

  TriMesh::HalfedgeHandle  hh;
  TriMesh::VertexHandle    v0, v1, v2, v3;

  hh = _mesh->halfedge_handle(_eh, 0);
  v3 = _mesh->to_vertex_handle(hh);
  hh = _mesh->next_halfedge_handle(hh);
  v0 = _mesh->to_vertex_handle(hh);
  hh = _mesh->halfedge_handle(_eh, 1);
  v1 = _mesh->to_vertex_handle(hh);
  hh = _mesh->next_halfedge_handle(hh);
  v2 = _mesh->to_vertex_handle(hh);


  /* These are the four points of the triangles incident to edge _eh
          a
         / \
        /   \
      b ----- c
        \   /
         \ /
          d
  */
  const TriMesh::Point& a = _mesh->point(v0);
  const TriMesh::Point& b = _mesh->point(v1);
  const TriMesh::Point& d = _mesh->point(v2);
  const TriMesh::Point& c = _mesh->point(v3);

  bool result = true;

  // INSERT CODE:
  // is the edge _eh delaunay or not?
  // -> circum-circle test of the four points (a,b,c,d)
  //--- start strip ---
  double matval[] = {
    a[0], a[1], a[0]*a[0]+a[1]*a[1], 1,
    b[0], b[1], b[0]*b[0]+b[1]*b[1], 1,
    c[0], c[1], c[0]*c[0]+c[1]*c[1], 1,
    d[0], d[1], d[0]*d[0]+d[1]*d[1], 1, 
  };
  Matrix4x4 mat(matval);
  
  result = !(mat.determinant() > 0);

  //--- end strip ---

  return result;
}

//-----------------------------------------------------------------------------

void
GP_Delaunay2DPlugin::
slotCreateInitialMesh()
{
  // create an inital mesh that consists of two triangles

  // add a tri mesh
  int id;
  emit addEmptyObject( DATA_TRIANGLE_MESH , id );

  BaseObjectData* object;
  PluginFunctions::getObject(  id , object );

  TriMeshObject * meshObject = PluginFunctions::triMeshObject( object );
  meshObject->target(true);
  meshObject->show();
  meshObject->setName("DelaunayMesh");
  meshObject->meshNode()->drawMode(ACG::SceneGraph::DrawModes::WIREFRAME);

  TriMesh * mesh = meshObject->mesh();

  // clear mesh first
  mesh->clear();

  TriMesh::Point p0 = TriMesh::Point(0, 0, 0);
  TriMesh::Point p1 = TriMesh::Point(1, 0, 0);
  TriMesh::Point p2 = TriMesh::Point(1, 1, 0);
  TriMesh::Point p3 = TriMesh::Point(0, 1, 0);

  // add 4 vertices
  TriMesh::VertexHandle vh[4];
  vh[0] = mesh->add_vertex(p0);
  vh[1] = mesh->add_vertex(p1);
  vh[2] = mesh->add_vertex(p2);
  vh[3] = mesh->add_vertex(p3);

  // add cones
  ACG::SceneGraph::GlutPrimitiveNode * node = getGlutPrimitiveNode(meshObject);
  ACG::Vec3d axis = ACG::Vec3d(0,0,1);
  ACG::Vec3d offset = ACG::Vec3d(0,0,-1); // add an offset to the cones position, since the position is not located at the apex put at the intersection of axis and base plate
  node->add_primitive(ACG::SceneGraph::GlutPrimitiveNode::CONE, p0+offset, axis, getRandomColor());
  node->add_primitive(ACG::SceneGraph::GlutPrimitiveNode::CONE, p1+offset, axis, getRandomColor());
  node->add_primitive(ACG::SceneGraph::GlutPrimitiveNode::CONE, p2+offset, axis, getRandomColor());
  node->add_primitive(ACG::SceneGraph::GlutPrimitiveNode::CONE, p3+offset, axis, getRandomColor());

  // add 2 initial triagles
  mesh->add_face(vh[0], vh[1], vh[2]);
  mesh->add_face(vh[0], vh[2], vh[3]);

  emit updatedObject(meshObject->id(), UPDATE_ALL);

  // set the pick mode to 2D delaunay mode
  PluginFunctions::pickMode("GP_Delaunay2D");

  // set 2d view
  slotSet2DView();
}

//-----------------------------------------------------------------------------

ACG::SceneGraph::GlutPrimitiveNode*
GP_Delaunay2DPlugin::
getGlutPrimitiveNode(TriMeshObject * _object)
{
  // get or add material node for primitive node
  ACG::SceneGraph::MaterialNode * material_node = 0;
  if( !_object->hasAdditionalNode("Delaunay2DPlugin", "PrimitiveMaterialNode" ) )
  {
    material_node = new ACG::SceneGraph::MaterialNode( _object->manipulatorNode(),  tr("Delaunay2D: Cone Material").toStdString() );

    if( !_object->addAdditionalNode(material_node, QString("Delaunay2DPlugin"), "PrimitiveMaterialNode") )
    {
      std::cerr << "Delaunay2DPlugin::getGlutPrimitiveNode(): could not add an primitive material node\n";
      return 0;
    }
  }
  else
    _object->getAdditionalNode( material_node,  "Delaunay2DPlugin", "PrimitiveMaterialNode" );


  ACG::SceneGraph::GlutPrimitiveNode * glut_node = 0;
  // get or add point node
  if( !_object->hasAdditionalNode("Delaunay2DPlugin", "GlutPrimitiveNode" ) )
  {
    glut_node = new ACG::SceneGraph::GlutPrimitiveNode( material_node, tr("Delaunay2D: Cones").toStdString());

    if( !_object->addAdditionalNode(glut_node,  QString("Delaunay2DPlugin"), "GlutPrimitiveNode") )
    {
      std::cerr << "Delaunay2DPlugin::getGlutPrimitiveNode(): could not add glut primitive node\n";
      return 0;
    }
    glut_node->clear();
    glut_node->drawMode(ACG::SceneGraph::DrawModes::SOLID_FACES_COLORED);
    glut_node->show();
  }
  else
    _object->getAdditionalNode( glut_node, "Delaunay2DPlugin", "GlutPrimitiveNode" );

  return glut_node;
}

//-----------------------------------------------------------------------------

ACG::Vec4f
GP_Delaunay2DPlugin::
getRandomColor()
{
  return ACG::Vec4f( (double)(rand()%255)/255.0, (double)(rand()%255)/255.0, (double)(rand()%255)/255.0, 1.0);
}

//-----------------------------------------------------------------------------

void
GP_Delaunay2DPlugin::
slotSet2DView()
{
  // set orthographic projection
  PluginFunctions::orthographicProjection();
  // set viewing direction to z axis
  PluginFunctions::viewingDirection(-0.35 * ACG::Vec3d(0.0, 0.0, 1.0), ACG::Vec3d(0.0, 1.0, 0.0));
  emit updateView();
}

//-----------------------------------------------------------------------------

#if QT_VERSION < 0x050000
  Q_EXPORT_PLUGIN2( gp_delaunay2DPlugin , GP_Delaunay2DPlugin );
#endif


