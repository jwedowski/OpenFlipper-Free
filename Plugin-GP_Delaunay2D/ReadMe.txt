Geometry Processing, Assignment 1
=================================

2D Delaunay triangulation & Voronoi diagrams

Important Notes:
---------------

1) Only submit the files that were changed by you in the exercise. In this
   assignment this will be the files:
	 GP_Delaunay2DPlugin.cc
	 MEMBERS.txt
   The MEMBERS.txt should contain names and matriculation numbers of all
   members of your group. Upload these files in a zip archive to the L2P.

2) Hand in your assignments in groups of 3-4 people. We will not grade
   solutions of groups with less than 3 people.

3) You have two weeks to prepare your solutions. Solutions need to be
   uploaded until Wednesday, May 17th at 1 pm.

Hints:
------

1) Extract the zip-file in the toplevel OpenFlipper directory.
   This creates a directory Plugin-GP_Delaunay2D with all relevant files.

2) Generate makefiles:
   (Linux)
   Call cmake .. from the build directory, e.g., build-release,
   to generate the makefiles for the new Plugin.

   (Windows)
   Use cmake-gui for configuring and generating the makefiles.


What you should implement:
--------------------------

Fill in the missing lines of code in the file GP_Delaunay2DPlugin.cc:

1) Re-establish the Delaunay property after inserting
   a new vertex into the triangulation.

   Hint: As boundary edges must not be flipped, check
   mesh->is_flip_ok(e)
   before flipping an edge e.
   This ensures that the resulting triangulation is valid.

2) Test whether an edge is Delaunay or not by
   implementing the circum-circle criterion.
