//=============================================================================
//
//  CLASS GP_Delaunay2DToolboxWidget - IMPLEMENTATION
//
//  $Date$
//
//=============================================================================

#include "GP_Delaunay2DToolboxWidget.hh"
#include <QtGui>

GP_Delaunay2DToolboxWidget::GP_Delaunay2DToolboxWidget(QWidget *parent)
    : QWidget(parent)
{
    setupUi(this);
}
