#ifndef GP_DELAUNAY2D_PLUGIN_HH
#define GP_DELAUNAY2D_PLUGIN_HH

#include <OpenFlipper/BasePlugin/BaseInterface.hh>
#include <OpenFlipper/BasePlugin/MouseInterface.hh>
#include <OpenFlipper/BasePlugin/PickingInterface.hh>
#include <OpenFlipper/BasePlugin/LoadSaveInterface.hh>
#include <OpenFlipper/BasePlugin/ToolboxInterface.hh>
#include <OpenFlipper/common/Types.hh>

#include <ObjectTypes/TriangleMesh/TriangleMesh.hh>
#include <ACG/Scenegraph/GlutPrimitiveNode.hh>

#include "./GP_Delaunay2DToolboxWidget.hh"

class GP_Delaunay2DPlugin : public QObject, BaseInterface, MouseInterface, PickingInterface, LoadSaveInterface, ToolboxInterface
{
  Q_OBJECT
  Q_INTERFACES(BaseInterface)
  Q_INTERFACES(MouseInterface)
  Q_INTERFACES(PickingInterface)
  Q_INTERFACES(LoadSaveInterface)
  Q_INTERFACES(ToolboxInterface)
  
  #if QT_VERSION >= 0x050000
  Q_PLUGIN_METADATA(IID "org.OpenFlipper.Plugins.Plugin-GP_Delaunay2D")
  #endif
  
  
  signals:
    
    void updateView();
    
    void updatedObject(int _identifier, const UpdateType _type);
    
    void addPickMode( const std::string &_mode );
    
    void addToolbox( QString _name  , QWidget* _widget );    

    void addEmptyObject( DataType _type, int& _id);
    
  private slots : // Open flipper slots
    
    void initializePlugin();
    
    void pluginsInitialized();

    void slotMouseEvent( QMouseEvent* _event );

  private slots : // my slots
    
    void slotCreateInitialMesh();
    
    void slotSet2DView();
  
  public :

    /// default constructor
    GP_Delaunay2DPlugin() {};
    
    ~GP_Delaunay2DPlugin() {};

    QString name() { return QString("GP_Delaunay2DPlugin"); };

    QString description() { return QString("Computes a two-dimensional Delaunay triangulation."); };

  private:
    
    /// Widget for Toolbox
    GP_Delaunay2DToolboxWidget * gui_;
    
    ACG::SceneGraph::GlutPrimitiveNode * getGlutPrimitiveNode(TriMeshObject * _object);
    
    void insertPoint(TriMeshObject * _meshObject, const bool _onEdge, const TriMesh::FaceHandle& _fh, const TriMesh::EdgeHandle& _eh, const TriMesh::Point& _p);
    
    bool isDelaunay(TriMesh * _mesh, TriMesh::EdgeHandle _eh);
    
    ACG::Vec4f getRandomColor();
};

#endif
